---
layout: job_page
title: "Business Development Team Lead"
---

#### **This job is being moved to [/positions/business-development-representative/](/positions/business-development-representative)**
{: .text-center}
<br>

As a Business Development Team Lead, you are a player-coach. As a Team Lead your job is threefold: (1) Help connect people interested in GitLab to the sales team or other appropriate channels, (2) lead from the front and train other members of the business development team, and (3) take on operational and administrative tasks to help the business development team perform and exceed expectations. You will be the source of knowledge and best practices amongst the Inbound BDRs, and will help to train, onboard and mentor new BDRs.


## Responsibilities

* Identifying where leads are in the sale and marketing funnel and taking the appropriate action.
* Passing qualified leads to the sales team and ensuring the sales team follows through.
* Training other members of the business development team to identify and pass qualified leads.
* Ensuring business development team members improve performance and abilities over time by providing coaching and feedback.
* Assist in interviewing, hiring, and onboarding new Business Development Representatives.
* Aide in managing the lead funnel from marketing qualified lead (MQL) to sales qualified opportunity (SQO).
* Work closely with the Sales and Business Development Manager to improve the lead management and qualification processes


## Requirements

* Excellent spoken and written English
* Experience in sales, marketing, or customer service for a technical product - leadership experience is highly preferred.
* Experience with CRM software (Salesforce preferred)
* Experience in sales operations and/or marketing automation software preferred
* Understanding of B2B software, Open Source software, and the developer product space is preferred
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been selling and marketing products since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Passionate about technology and learning more about GitLab
* Be ready to learn how to use GitLab and Git
* You share our [values](/handbook/values), and work in accordance with those values.
