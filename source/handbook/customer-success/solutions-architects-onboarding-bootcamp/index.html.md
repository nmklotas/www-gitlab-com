---
layout: markdown_page
title: "Solutions Architect Onboarding"
---

When a GitLab Solutions Architect starts they will need to complete an intensive bootcamp.

Their manager will create an onboarding issue to track their progress by copying the content from the
[raw Markdown source](https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/source/handbook/customer-success/solutions-architects-onboarding-bootcamp/index.html.md).

# Week 1 : General Onboarding

* [ ] [General Onboarding](https://about.gitlab.com/handbook/general-onboarding/)
 
  
# Week 2: Technical Onboarding

## GIT Basics
**Goal:** Become familiar with git and GitLab basics

* [ ] [Download Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html)  
* [ ] [About Version Control](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit) 
* [ ] [Try Git](https://www.codeschool.com/account/courses/try-git)

## GitLab General Knowledge 
**Goal:** Understand how GitLab is used by our customers

* [ ]  [GitLab Flow](https://www.youtube.com/watch?v=UGotqAUACZA)
* [ ] Cover the Beginner and Intermediate sections in [GitLab University](https://university.gitlab.com/)
* [ ]  All the [GitLab Basics](http://docs.gitlab.com/ce/gitlab-basics/README.html) that you don't feel comfortable with. Pay special attention to:
  * [ ] [Command line basics](http://docs.gitlab.com/ce/gitlab-basics/command-line-commands.html)
  * [ ] [Creating SSH keys](http://docs.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html)
  * [ ] [Creating a branch](http://docs.gitlab.com/ce/gitlab-basics/create-branch.html)
  * [ ] [Creating a merge request](http://docs.gitlab.com/ce/gitlab-basics/add-merge-request.html)
* [ ]  Take a look at how the different [GitLab versions compare](https://about.gitlab.com/products/)
* [ ] Look at the [User Training](https://gitlab.com/gitlab-org/University/tree/master/training) we use for our customers.  Pay special attention to:
  * [ ] `env_setup.md`
  * [ ] `feature_branching.md`
  * [ ] `explore_gitlab.md`
  * [ ] `stash.md`
  * [ ] `git_log.md`

## GitLab Product Knowledge
**Goal:** Get familiar with the products & services GitLab offers and how we deliver them to customers

* [ ] Learn the differences between [CE and EE](https://about.gitlab.com/pricing/)
* [ ] Review the various [features of GitLab](https://about.gitlab.com/features/)

Perform each of the following [Installation Methods](https://about.gitlab.com/installation/) on your preferred test environment you chose above:
* [ ]  Install via [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/)
* [ ]  Populate with some test data: User account, Project, Issue
* [ ]  Backup using our [Backup rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
* [ ]  Install via [Docker](https://docs.gitlab.com/ce/install/docker.html)
* [ ]  Restore backup to your Docker VM using our [Restore rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#restore-a-previously-created-backup)

# Week 3: Team Onboarding

## GitLab Support
**Goal:** Get to know GitLab Support

* [ ] Pair with a support team member 
* [ ] Identify support engineer (appropriate to timezone)
* [ ] Pair on 3-5 calls 

**Goal:** Get to know Zendesk

* [ ] Complete Zendesk Agent training (allow 40 minutes for completion)
  * [ ]  Navigate to [Zendesk university](https://university.zendesk.com/#/purchase/category/34942) and order the **"Agents: Zendesk Fundamentals Online"** course
  * [ ]  Add the **"Agents: Zendesk Fundamentals Online"** course to your cart and click "Proceed to Checkout"
      - Follow the prompts and finalize your order.
      - You'll receive an email with information on accessing the Zendesk course
  * [ ] Proceed to complete the **"Agents: Zendesk Fundamentals Online"** course
  * [ ] Review additional Zendesk resources
    * [ ] [UI Overview](https://support.zendesk.com/hc/en-us/articles/203661806-Introduction-to-the-Zendesk-agent-interface)
    * [ ] [Updating Tickets](https://support.zendesk.com/hc/en-us/articles/212530318-Updating-and-solving-tickets)
    * [ ] [Working with Tickets](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets) *Read: avoiding agent collision.*

**Goal:** Get to know how GitLab uses Zendesk in Support

* [ ]  Dive into our ZenDesk support process by reading [how to handle tickets](https://about.gitlab.com/handbook/support/onboarding/#handling-tickets)
* [ ]  Learn about the [tiered support system](https://about.gitlab.com/handbook/support/#tiered-support)
* [ ]  Read about [Escalation](https://about.gitlab.com/handbook/support/onboarding/#create-issues)

## GitLab Sales
**Goal:** Learn how GitLab Sales as a whole operates
* [ ]  Study the [Sales Handbook](https://about.gitlab.com/handbook/sales/) 
* [ ]  Familiarize yourself with our [Lead Qualification Process](https://about.gitlab.com/handbook/marketing/business-development/#mql) 
 * [ ]  Learn how to give a [Idea to Production Demo](https://about.gitlab.com/handbook/product/i2p-demo/)
* [ ]  Study the [GitLab EE product qualification questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)
* [ ]  [Understanding of Gitlab HA and Gitlab GEO](https://about.gitlab.com/high-availability/)
 
In the Sales Folder, familiarize yourself with:
* [ ] Our [Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit)

## Other Teams
**Goal:** Get to know how other teams at GitLab help our product and customers

* [ ]  Take a look at the GitLab.com [Team page](https://about.gitlab.com/team/) to find the resident experts in their fields
* [ ]  Understand what's in the pipeline and proposed features at GitLab's [Direction Page](https://about.gitlab.com/direction/)
* [ ]  Practice searching issues and filtering using [labels](https://gitlab.com/gitlab-org/gitlab-ce/labels) to find existing feature proposals and bugs
  - If raising a new issue always provide a relevant label and a link to the relevant ticket in Zendesk
* [ ]  Take a look at the existing [issue templates](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#issue-tracker) to see what is expected

## Self-assessments
**Goal:** Self-assess your progress in general Git knowledge as well as GitLab product knowledge 

 * [ ]   [General SCM/Git knowledge and DevOps](https://goo.gl/forms/BjNezQx487d7uFpz2)
 * [ ]  [Gitlab CI/CD](https://goo.gl/forms/KMj3U6xfLfo4sBnp1)

# Week 4: GitLab SME

## GitLab SME Information
**Goal:** Start becoming a GitLab SME

* [ ]  Read about the [GitLab Workflow](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/) 
* [ ]  Read about [the GitLab 10.0 released features](https://about.gitlab.com/2017/09/22/gitlab-10-0-released/)
* [ ]  Learn more about all [GitLab Products](https://about.gitlab.com/products/)
* [ ]  Read details on [Future Releases](https://about.gitlab.com/direction/#future-releases)
* [ ]  Learn detailed information about [EE features for upgrade from CE](https://about.gitlab.com/comparison/gitlab-ce-vs-gitlab-ee.html)
* [ ]  Learn the difference between [EE Starter to EE Premium](https://about.gitlab.com/comparison/gitlab-ees-vs-gitlab-eep.html)  
* [ ]  Learn [who's who in Product](https://about.gitlab.com/handbook/product/#who-to-talk-to-for-what)

## In-depth product Knowledge
**Goal:** Learn detailed information about maintaining and administering GitLab using GitLab rake commands

* [ ]  Learn about environment Information and [maintenance checks](http://docs.gitlab.com/ce/raketasks/maintenance.html)
* [ ]  Learn about the [GitLab check](http://docs.gitlab.com/ce/raketasks/check.html) rake command
* [ ]  Learn about GitLab Omnibus commands (`gitlab-ctl`)
  * [ ]  [GitLab Status](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#get-service-status)
  * [ ]  [Starting and stopping GitLab services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-and-stopping)
* [ ]  Starting a [rails console](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#invoking-rake-tasks)
* [ ]  Get to know the [GitLab API](https://docs.gitlab.com/ee/api/README.html), its capabilities and shortcomings
* [ ]  Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)

## Self-assessments
**Goal:** Self-assess your progress in detailed GitLab Product Knowledge

 * [ ]   [Installing/configuring GitLab and Gitlab best practices](https://goo.gl/forms/zN5OwQIrulR8g6G32)
* [ ]  [Gitlab HA and Gitlab GEO](https://goo.gl/forms/qkVaOMLGKF9k9jcf1)

# Week 5 & Onward
## Understanding the Competition
**Goal:** Learned detailed information about our competition and why we believe GitLab to be a superior solution

* [ ] [Competition](https://about.gitlab.com/comparison/)
  * [ ]  [GitHub](http://github.com)
  * [ ] [Bitbucket](https://bitbucket.org/) and [Atlassian](https://www.atlassian.com/)
  * [ ] [Perforce](https://www.perforce.com/)
  * [ ] [Microsoft Team Foundation Server (TFS)](https://www.visualstudio.com/tfs/)
  * [ ] [Jenkins](https://jenkins.io/) and [CloudBees](https://www.cloudbees.com/)
  * [ ] Other SCM, CI/CD and Deployment tools

## Integrations
**Goal:** Understand Integrations of other products (including competitive products) with GitLab

* [ ]  [Jira](https://docs.gitlab.com/ee/user/project/integrations/jira.html)
* [ ]  [Jenkins](https://docs.gitlab.com/ee/integration/jenkins.html)
* [ ]  Authentication via [LDAP/OAuth](https://docs.gitlab.com/ee/integration/oauth_provider.html)

### Custom Integrations
**Goal:** Understand how developers and customers can produce custom integrations to GitLab

If you want to integrate with GitLab there are three possible paths you can take:
*  [ ] [Utilizing webhooks](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html) - If you want to reflect information from GitLab or initiate an action based on a specific activity that occurred on GitLab you can utilize the existing infrastructure of our webhooks. To read about setting up webhooks on GitLab visit this page.
*  [ ] [API integration](https://docs.gitlab.com/ee/api/README.html) - If you're seeking a richer integration with GitLab, the next best option is to utilize our ever-expanding API. The API contains pretty much anything you'll need, however, if there's an API call that is missing we'd be happy to explore it and develop it.
*  [ ] [Project Services](https://docs.gitlab.com/ce/user/project/integrations/project_services.html) - Project services give you the option to build your product into GitLab itself. This is the most advanced and complex method of integration but is by far the richest. It requires you to add your integration to GitLab's code base as a standard contribution. If you're thinking of creating a project service, the steps to follow are similar to any contribution to the GitLab codebase.


## Migrations
**Goal:** Understand how migrations from other Version Control Systems to Git & GitLab work

* [ ]  [SVN](https://git-scm.com/book/en/v1/Git-and-Other-Systems-Git-and-Subversion)
* [ ]  Learn how to [migrate from SVN to Git](https://docs.gitlab.com/ee/workflow/importing/migrating_from_svn.html)


## Self-assessments
**Goal:** Self-assess your progress in Integrations & Migrations
* [ ]  [Data migration & integrations](https://goo.gl/forms/Ld2mg6LjmcGgGqkj1)

## Demo Scenarios

You will be required to demo to team all demo scenarios specific to customer/prospect requirements in mock style showing [business value](https://docs.google.com/document/d/1kSVUNM4u6KI8M9FxoyiUbHEHAHIi34iiY25NhMxLucc/edit) 

Final certification
* [ ]  Sales role play (demo scenarios)
* [ ]  Comparison of Gitlab editions (key differentiators and business drivers)
* [ ]  Ecosystem and how Gitlab interacts/compete with other tools
* [ ]  Presentation on Idea to Production
* [ ]  Interactive demo paired with another SA

